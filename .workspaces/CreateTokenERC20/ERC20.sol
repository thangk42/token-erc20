pragma solidity^0.4.25;

// ----------------------------------------------------------------------------
// ERC Token Standard #20 Interface
// https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20-token-standard.md
// ----------------------------------------------------------------------------
interface ERC20 {

    function totalSupply() public  view returns (uint256);
    function balanceOf(address _owner)  public  view returns (uint256 balance);
    function transfer(address _to, uint256 _value)  public  returns (bool success);
    function transferFrom(address _from, address _to, uint256 _value)  public  returns (bool success);
    function approve(address _spender, uint256 _value)  public  returns (bool success);
    function allowance(address _owner, address _spender)  public  view returns (uint256 remaining);
  
   
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
  
}